package com.knubisoft.base.arrays;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Arrays.stream;

public class ArraysTasksImpl implements ArraysTasks {

    @Override
    public int[] reverse(int[] array) {
        List<Integer> list = stream(array).boxed().collect(Collectors.toList());
        Collections.reverse(list);
        return list.stream().mapToInt(Integer::intValue).toArray();
    }

    @Override
    public int[] mergeArrays(int[] array1, int[] array2) {
        return IntStream.concat(stream(array1), stream(array2)).toArray();
    }

    @Override
    public int[] findMax3InArray(int[] array) {
        if (array.length < 3) {
            return array;
        }
        List<Integer> list = stream(array).boxed().sorted().collect(Collectors.toList());
        Collections.reverse(list);
        return list.stream().mapToInt(Integer::intValue).limit(3).toArray();
    }

    @Override
    public int findLongestIncreasingContinuesSubsequence(int[] array) {
        if (array.length < 1) {
            return 0;
        }
        int subsequence = 0;
        int temp = 1;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                temp = 1;
            } else if (array[i] == array[i + 1]) {
                temp = 1;
            } else {
                temp++;
            }
            if (subsequence < temp) {
                subsequence = temp;
            }
        }
        return subsequence;
    }

    @Override
    public int sumOfAllUniqueElements(int[] array) {
        Set<Integer> set = new HashSet(array.length);
        for (int a : array) {
            set.add(a);
        }
        return set.stream().mapToInt(Integer::intValue).sum();
    }

    @Override
    public int[] moveZeroes(int[] array) {
        int[] result = new int[array.length];
        int count = 0;
        for (int i = 0; i < array.length; i++) {

            if (array[i] != 0) {
                result[count] = array[i];
                count++;
            }
        }
        return result;
    }

    @Override
    public int findFinalValue(int[] nums, int original) {

        boolean check = true;
        while (check) {
            for (int i = 0; i < nums.length; i++) {
                if (nums[i] == original) {
                    original = original * 2;
                    i = -1;
                }

            }
            check = false;
        }
        return original;
    }

    @Override
    public String longestCommonPrefix(String[] words) {
        int size = words.length;
        if (size == 0)
            return "";

        if (size == 1)
            return words[0];

        Arrays.sort(words);

        int end = Math.min(words[0].length(), words[size - 1].length());

        int i = 0;
        while (i < end && words[0].charAt(i) == words[size - 1].charAt(i))
            i++;

        return words[0].substring(0, i);
    }

    @Override
    public int missingNumber(int[] array) {
        int result = array.length;
        List<Integer> list = stream(array).boxed().sorted().collect(Collectors.toList());
        array = list.stream().mapToInt(Integer::intValue).toArray();
        for (int i = array.length - 1; i != -1; i--) {
            if (array[i] == result) {
                result--;
            } else {
                break;
            }
        }
        return result;
    }


    @Override
    public boolean containsDuplicate(int[] array) {
        Set set = new HashSet();
        for (int j : array) {
            set.add(j);
        }
        if (set.size() == array.length){
            return false;
        }
        else return true;
    }
}
